int lcd_open(const char *str)
{
	g_fb_fd = open(str, O_RDWR);
	
	if(g_fb_fd<0)
	{
			printf("open lcd error\n");
			return -1;
	}

	g_pfb_memory  = (int *)mmap(	NULL, 					//Ó³ÉäÇøµÄ¿ªÊŒµØÖ·£¬ÉèÖÃÎªNULLÊ±±íÊŸÓÉÏµÍ³Ÿö¶šÓ³ÉäÇøµÄÆðÊŒµØÖ·
									FB_SIZE, 				//Ó³ÉäÇøµÄ³€¶È
									PROT_READ|PROT_WRITE, 	//ÄÚÈÝ¿ÉÒÔ±»¶ÁÈ¡ºÍÐŽÈë
									MAP_SHARED,				//¹²ÏíÄÚŽæ
									g_fb_fd, 				//ÓÐÐ§µÄÎÄŒþÃèÊöŽÊ
									0						//±»Ó³Éä¶ÔÏóÄÚÈÝµÄÆðµã
								);

	return g_fb_fd;

}
int lcd_draw_jpg_in_jpg(unsigned int x,unsigned int y,const char *pjpg_path,char *pjpg_buf,unsigned int jpg_buf_size)  
{
	/*¶šÒåœâÂë¶ÔÏó£¬ŽíÎóŽŠÀí¶ÔÏó*/
	struct 	jpeg_decompress_struct 	cinfo;
	struct 	jpeg_error_mgr 			jerr;	
	
	char 	*pcolor_buf = g_color_buf;
	char 	*pjpg;
	
	unsigned int 	i=0;
	unsigned int	color =0;
	unsigned int	count =0;
	
	unsigned int 	x_s = x;
	unsigned int 	x_e ;	
	unsigned int 	y_e ;
	unsigned int	y_n	= y;
	unsigned int	x_n	= x;
	
			 int	jpg_fd;
	unsigned int 	jpg_size;

	if(pjpg_path!=NULL)
	{
		/* ÉêÇëjpg×ÊÔŽ£¬ÈšÏÞ¿É¶Á¿ÉÐŽ */	
		jpg_fd=open(pjpg_path,O_RDWR);
		
		if(jpg_fd == -1)
		{
		   printf("open %s error\n",pjpg_path);
		   
		   return -1;	
		}	
		
		/* »ñÈ¡jpgÎÄŒþµÄŽóÐ¡ */
		jpg_size=file_size_get(pjpg_path);	

		/* ÎªjpgÎÄŒþÉêÇëÄÚŽæ¿ÕŒä */	
		pjpg = malloc(jpg_size);

		/* ¶ÁÈ¡jpgÎÄŒþËùÓÐÄÚÈÝµœÄÚŽæ */		
		read(jpg_fd,pjpg,jpg_size);
	}
	else
	{
		jpg_size = jpg_buf_size;
		
		pjpg = pjpg_buf;
	}

	/*×¢²á³öŽíŽŠÀí*/
	cinfo.err = jpeg_std_error(&jerr);

	/*ŽŽœšœâÂë*/
	jpeg_create_decompress(&cinfo);

	/*Ö±œÓœâÂëÄÚŽæÊýŸÝ*/		
	jpeg_mem_src(&cinfo,pjpg,jpg_size);
	
	/*¶ÁÎÄŒþÍ·*/
	jpeg_read_header(&cinfo, TRUE);

	/*¿ªÊŒœâÂë*/
	jpeg_start_decompress(&cinfo);	
	
	
	x_e	= x_s+cinfo.output_width;
	y_e	= y  +cinfo.output_height;	

	/*¶ÁœâÂëÊýŸÝ*/
	while(cinfo.output_scanline < cinfo.output_height )
	{		
		pcolor_buf = g_color_buf;
		
		/* ¶ÁÈ¡jpgÒ»ÐÐµÄrgbÖµ */
		jpeg_read_scanlines(&cinfo,(JSAMPARRAY)&pcolor_buf,1);
		
		for(i=0; i<cinfo.output_width; i++)
		{
			/* »ñÈ¡rgbÖµ */
			color = 		*(pcolor_buf+2);
			color = color | *(pcolor_buf)<<8;
			color = color | *(pcolor_buf)<<16;
			
			if(++x == LCD_WIDTH)
					break;
			
			if((y_n-y)>=240 && (x_n-x)>=320)
				break;
			
			/* ÏÔÊŸÏñËØµã */
			lcd_draw_point(x_n,y_n,color);
			
			pcolor_buf +=3;
			
			x_n++;
		}
		
		/* »»ÐÐ */
		y_n++;			
		
		x_n = x_s;
		
	}		

				
	/*œâÂëÍê³É*/
	jpeg_finish_decompress(&cinfo);
	jpeg_destroy_decompress(&cinfo);

	if(pjpg_path!=NULL)
	{
		/* ¹Ø±ÕjpgÎÄŒþ */
		close(jpg_fd);	
		
		/* ÊÍ·ÅjpgÎÄŒþÄÚŽæ¿ÕŒä */
		free(pjpg);		
	}


	
	return 0;
}
void lcd_close(void)
{
	
	/* 取消内存映射 */
	munmap(g_pfb_memory, FB_SIZE);
	
	/* 关闭LCD设备 */
	close(g_fb_fd);
}


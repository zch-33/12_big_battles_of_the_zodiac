
#include<stdio.h>
#include<stdbool.h>
#include<stdlib.h>
#include<string.h>
#include<time.h>
#include <termio.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <arpa/inet.h>
#include <api_v4l2.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
int  mp[4][4],score;
bool map[4][4];
bool cal[4][4];


void DeBug()
{
		int i,j;
    for(i=0;i<4;++i){
        for(j=0;j<4;++j)
            printf("%d\n",map[i][j]);
        
    }
    
	
    for( i=0;i<4;++i){
        for( j=0;j<4;++j)
            printf("%d\n",mp[i][j]);
        
    }
}
void print()
{	
	int i,j;

    for(j=0;j<4;++j)
            printf("----");
        printf("\n");
    for(i=0;i<4;++i){
        for(j=0;j<4;++j){
            printf("|");
            if(map[i][j]){
                if(score<mp[i][j])
                    score=mp[i][j];
                printf("%3d",mp[i][j]);
            }else
                printf("   ");

        }
        printf("|\n");
        for( j=0;j<4;++j)
            printf("----");
        printf("\n");
    }
    printf("w : up  s : down  a : left  d : right\n");
    printf("push 'r' to start a new game\npush 'e' to exit game\n");
}
void MoveUp()
{
	int i,j,k;
    bool flag;
    for( i=1;i<4;++i){
        for( j=0;j<4;++j){
            if(!map[i][j])
                continue;
            flag=false;
            int pos=i;
            for(k=i-1;k>=0;--k){
                if(!map[k][j]){
                    pos=k;
                    continue;
                }
                if((mp[k][j]==mp[i][j])&&(!cal[k][j])){
                    mp[k][j]+=mp[i][j];
                    cal[k][j]=true;
                    flag=true;
                    map[i][j]=false;
                }
                else
                    break;
            }
            if(!flag){
                mp[pos][j]=mp[i][j];
                map[i][j]=false;
                map[pos][j]=true;
            }
        }
    }
}
void MoveDown()
{
    bool flag;
    int i,j,k;
    for(i=2;i>=0;--i){
        for(j=0;j<4;++j){
            if(!map[i][j])
                continue;
            flag=false;
            int pos=i;
            for(k=i+1;k<4;++k){
                if(!map[k][j]){
                    pos=k;
                    continue;
                }
                if((mp[k][j]==mp[i][j])&&(!cal[k][j])){
                    mp[k][j]+=mp[i][j];
                    cal[k][j]=true;
                    flag=true;
                    map[i][j]=false;
                }
                else
                    break;
            }
            if(!flag){
                mp[pos][j]=mp[i][j];
                map[i][j]=false;
                map[pos][j]=true;
            }
        }
    }
}
void MoveRight()
{
    int i,j,k;
    bool flag;
    for(i=0;i<4;++i){
        for(j=2;j>=0;--j){
            if(!map[i][j])
                continue;
            flag=false;
            int pos=j;
            for(k=j+1;k<4;++k){
                if(!map[i][k]){
                    pos=k;
                    continue;
                }
                if((mp[i][k]==mp[i][j])&&(!cal[i][k])){
                    mp[i][k]+=mp[i][j];
                    cal[i][k]=true;
                    flag=true;
                    map[i][j]=false;
                }
                else
                    break;
            }
            if(!flag){
                mp[i][pos]=mp[i][j];
                map[i][j]=false;
                map[i][pos]=true;
            }
        }
    }
}
void MoveLeft()
{
    int i,j,k;
    bool flag;
    for(i=0;i<4;++i){
        for(j=1;j<4;++j){
            if(!map[i][j])
                continue;
            flag=false;
            int pos=j;
            for(k=j-1;k>=0;--k){
                if(!map[i][k]){
                    pos=k;
                    continue;
                }
                if((mp[i][k]==mp[i][j])&&(!cal[i][k])){
                    mp[i][k]+=mp[i][j];
                    cal[i][k]=true;
                    flag=true;
                    map[i][j]=false;
                }
                else
                    break;
            }
            if(!flag){
                mp[i][pos]=mp[i][j];
                map[i][j]=false;
                map[i][pos]=true;
            }
        }
    }
}
void Init()
{
		int i,j;
    srand(time(NULL));
    int x,y,v,now=0;
    for(i=0;i<4;++i)
    for(j=0;j<4;++j)
        if(!map[i][j])
            now++;
    if(now==0)
        return;
    int cnt=rand()%now,n=0;
    bool flag=false;
    for(i=0;i<4;++i){
        flag=false;
        for(j=0;j<4;++j)
        if(!map[i][j]){
            if(cnt==n){
                x=i;
                y=j;
                flag=true;
                break;
            }else
                n++;
        }
        if(flag)
            break;
    }
    v=rand()%2;
    if(v)
        v=4;
    else
        v=2;
    mp[x][y]=v;
    map[x][y]=true;
}
bool gameover()
{
   int i,j;
    for(i=0;i<4;++i){
        for(j=0;j<4;++j){
            if(!map[i][j])
                return false;
            if(i>0){
                if(mp[i-1][j]==mp[i][j])
                    return false;
            }
            if(j>0)
                if(mp[i][j-1]==mp[i][j])
                    return false;
        }
    }
    return true;
}
void Lose()
{
    printf("*******************\n");
    printf("**** GAME OVER ****\n");
    printf("**** SCORE:%3d ****\n",score);
    printf("*******************\n\n");
    printf("Push any button to continue\n");
	lcd_draw_jpg(350,40,"over.jpg",NULL,400*400*4,0);
}
void Win()
{
    printf("*******************\n");
    printf("**** YOU  WIN! ****\n");
    printf("*******************\n\n");
    printf("Push any button to continue\n");
	lcd_draw_jpg(0,0,
			"sss.jpg",
			NULL,
			800*480*4,
			0);
}

void start()
{
    char b[13][10]={"0.jpg","2.jpg","4.jpg","8.jpg",
		"16.jpg","32.jpg","64.jpg","128.jpg","256.jpg",
		"512.jpg","1024.jpg","2048.jpg","4096.jpg"};
	char sc[10][10]={"s0.jpg","s1.jpg","s2.jpg","s3.jpg",
		"s4.jpg","s5.jpg","s6.jpg","s7.jpg","s8.jpg",
		"s9.jpg"};
    printf("********************\n");
    printf("* game start glhf~ *\n");
    printf("********************\n");
    int x=5e8;
	
    bool flag=false;
    while(x--);
    memset(mp,0,sizeof(mp));
    memset(map,false,sizeof(map));
    score=0;
    char ch;
    int ret,min_x,min_y,i,j;
   /* 打开lcd设备 */
	if(-1==lcd_open("/dev/fb0"))
	{
		return ;		
	}

    while(!gameover())
    {
        memset(cal,false,sizeof(cal));
        int cnt=0;
       
        Init();
        print();
	
		for(i=0;i<4;i++)
			for(j=0;j<4;j++)
			{	
				if(map[i][j])
				ret=getscore(mp[i][j]);
				else
				ret=0;
				getmin(&min_x,&min_y,i*4+j);
				printf("%d %d\n",min_x,min_y);
				printf("%d\n",ret);
				lcd_draw_jpg(min_x,min_y,
					b[ret],
					NULL,
					97*97*4,
					0);
				
			}
		lcd_draw_jpg(110,360,sc[score/1000],NULL,64*64*4,0);
		lcd_draw_jpg(110+58,360,sc[score%1000/100],NULL,64*64*4,0);
		lcd_draw_jpg(110+58*2,360,sc[score%100/10],NULL,64*64*4,0);
		lcd_draw_jpg(110+58*3,360,sc[score%10],NULL,64*64*4,0);
        DeBug();
        ch=ts();
		printf("%c%c%c%c\n",ch,ch,ch,ch);
        switch (ch)
        {
            case 'w':MoveUp();break;
            case 's':MoveDown();break;
            case 'a':MoveLeft();break;
            case 'd':MoveRight();break;
        }
        if(score>=4096)
		{
            flag=true;
			Win();
            break;
        }
		else
			flag=false;
    }
	//if(ch == 'q') return 0;
	if(gameover())
	{
		flag=false;
		Lose();
	}

	//Lose();
    if(flag)  //ying
	{
		score=0;
		ch = ts2();
		if(ch=='m')
			start();
	}
    else   //shu
	{
		score=0;
		ch = ts2();
		if(ch == 'e')
			start();
		else
		{
			ch = ts2();
			while(ch!='e')
				ch = ts2();
			start();
		}
	}
}


void fun2048()
{
    start();

    lcd_close();
}

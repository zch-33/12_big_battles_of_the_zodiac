#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <stdlib.h>
#include <sys/stat.h>


unsigned long file_size_get(const char *pfile_path)
{
	unsigned long filesize = -1;	
	struct stat statbuff;
	
	if(stat(pfile_path, &statbuff) < 0)
	{
		return filesize;
	}
	else
	{
		filesize = statbuff.st_size;
	}
	
	return filesize;
}

void getmin(int *min_x,int *min_y,int n)
{
	switch(n)
	{
	case 0:  *min_x=350; *min_y=40;break;
	case 1:  *min_x=450; *min_y=40;break;
	case 2:  *min_x=550; *min_y=40;break;
	case 3:  *min_x=650; *min_y=40;break;
	case 4:  *min_x=350; *min_y=140;break;
	case 5:  *min_x=450; *min_y=140;break;
	case 6:  *min_x=550; *min_y=140;break;
	case 7:  *min_x=650; *min_y=140;break;
	case 8:  *min_x=350; *min_y=240;break;
	case 9:  *min_x=450; *min_y=240;break;
	case 10: *min_x=550; *min_y=240;break;
	case 11: *min_x=650; *min_y=240;break;
	case 12: *min_x=350; *min_y=340;break;
	case 13: *min_x=450; *min_y=340;break;
	case 14: *min_x=550; *min_y=340;break;
	case 15: *min_x=650; *min_y=340;break;
	}
}
int getscore(int score)
{

	switch(score)
	{
	case 0:   return 0;break;
	case 2:   return 1;break;
	case 4:   return 2;break;
	case 8:   return 3;break;
	case 16:  return 4;break;
	case 32:  return 5;break;
	case 64:  return 6;break;
	case 128: return 7;break;
	case 256: return 8;break;
	case 512:  return 9;break;
	case 1024: return 10;break;
	case 2048: return 11;break;
	case 4096: return 12;break;
	}

}

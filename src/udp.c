
/*************************************************
					系统头文件
 *************************************************/
#include <sys/types.h>			/* See NOTES */
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <linux/input.h>
#include <linux/in.h>
#include <stdlib.h>
#include <linux/socket.h>


/*************************************************
				   变量和函数声明
 *************************************************/
static int 		g_socket_fd;
static struct 	sockaddr_in g_local_addr;
static struct 	sockaddr_in g_dest_addr;

int udp_open(int local_port)
{
	int rt;
	
	/* 创建通信套接字 */
	g_socket_fd = socket(AF_INET,SOCK_DGRAM,0);
	
	if(g_socket_fd == -1)
	{
		printf("[udp_open]:create socket error!\n");
		
		return -1;			
	}

	/* 绑定通信ip与端口 */
	g_local_addr.sin_family 		= AF_INET;
	g_local_addr.sin_port 			= htons(local_port);
	g_local_addr.sin_addr.s_addr 	= htonl(INADDR_ANY);

	rt = bind(g_socket_fd,(struct sockaddr *)&g_local_addr,sizeof(struct sockaddr_in));
	
	if(rt)
	{
		printf("[udp_open]:bind socket error!\n");
		
		return -1;			
	}	
	
	return 0;
}

int udp_send(const char *dest_addr,int dest_port,char *pudp_send_buf,int udp_send_length)
{
	g_dest_addr.sin_family = AF_INET;
	g_dest_addr.sin_port = htons(dest_port);
	g_dest_addr.sin_addr.s_addr = inet_addr(dest_addr);		
	
	return 	sendto(	g_socket_fd, 
					pudp_send_buf, 
					udp_send_length, 
					0,
					(struct sockaddr *)&g_dest_addr, 
					sizeof(struct sockaddr_in));	
}

int udp_recv(char *pudp_recv_buf,int udp_recv_length)
{
	int socket_length;
	
	return recvfrom(	g_socket_fd, 
						pudp_recv_buf,
						udp_recv_length, 
						0,
						(struct sockaddr *)&g_dest_addr, 
						&socket_length);	
	
}

int udp_send_file(const char *dest_addr,int dest_port,const char *p_file_path)
{
	/* 文件描述符 */
	int fd=-1;

	/* 文件大小 */	
	int file_size=0;
	
	char *pfile_memory=NULL;
	
	fd=open(p_file_path, O_RDONLY, 0777);
	
	if(-1 == fd)
		return -1;
	
	file_size = file_size_get(p_file_path);
	
	pfile_memory =(char *)malloc(file_size);
	
	read(fd,pfile_memory,file_size);
	
	udp_send(dest_addr,dest_port,pfile_memory,file_size);
	
	free(pfile_memory);
	
	close(fd);
	
	return 0;
}

int udp_close(void)
{
	if(g_socket_fd)
	{
		close(g_socket_fd);
	}
	
	return 0;	
}
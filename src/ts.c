#include <stdio.h>
#include <fcntl.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <stdbool.h>
#include <strings.h>
#include <pthread.h>
#include <semaphore.h>

#include <sys/stat.h>
#include <sys/wait.h>
#include <sys/types.h>

#include <linux/input.h>
int x,nx,y,ny;
char ts()
{
	int fd = open("/dev/input/event0", O_RDONLY);

	struct input_event buf;
	
	bool x_done=false;
	bool y_done=true;
	char s;
	int flag=2;
	while(1)
	{
		bzero(&buf, sizeof(buf));
		read(fd, &buf, sizeof(buf));
		
		
		if(buf.type == EV_ABS)
		{
			switch(buf.code)
			{
			case ABS_X:
				if(y_done)
				{	
					if(flag!=0)
					{
						x=buf.value;
						printf("%d\n, ", x);
						flag--;
					}
					else
					nx=buf.value;
					printf("(%d, ", buf.value);
					x_done = true;
					y_done = false;
				}
				break;
			case ABS_Y:
				if(x_done)
				{
					if(flag!=0)
					{
						y=buf.value;
						printf("%d\n, ", y);
						flag--;
					}
					else
					ny=buf.value;
					printf("%d)\n", buf.value);
					y_done = true;
					x_done = false;
				}
				break;
			}
		}

		// 类按钮事件type: EV_KEY
		// 触摸屏压力值code: BTN_TOUCH
		// 手指松开时，压力值的value: 0
		
		if(buf.type == EV_KEY)
		{
			if(buf.code == BTN_TOUCH && buf.value == 0)
			   break;
		}
		
	}

	close(fd);
	
	/*滑屏算法*/
	if(nx>x)
	{
		if(ny>y)
		{
			if( abs(nx-x)>abs(ny-y) )
			{	
				s='d';
				printf("d\n %d %d",nx-x,ny-y);
			}
			else
			{	
				s='s';
				printf("s\n %d %d",nx-x,ny-y);	
			}
		}
		else
		{
			if( abs(nx-x)>abs(ny-y) )
			{
				s='d';
				printf("d\n %d %d",nx-x,ny-y);
			}
			else
			{
				s='w';
				printf("w\n %d %d",nx-x,ny-y);	
			}
		}
	}
	else
	{
		if(ny>y)
		{
			if( abs(nx-x)>abs(ny-y) )
			{
				s='a';
				printf("a\n %d %d",nx-x,ny-y);
			}
			else
			{
				s='s';
				printf("s\n %d %d",nx-x,ny-y);	
			}
		}
		else
		{
			if( abs(nx-x)>abs(ny-y) )
			{
				s='a';
				printf("a\n %d %d",nx-x,ny-y);
			}
			else
			{
				s='w';
				printf("w\n %d %d",nx-x,ny-y);	
			}
		}
	}
	return s;
}

